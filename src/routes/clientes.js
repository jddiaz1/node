const express = require("express");
const clienteSchema = require("../models/clientes");

const router = express.Router();

//create cliente

router.post("/clientes", (req, res) => {
  const cliente = clienteSchema(req.body);
  cliente
    .save()
    .then((data) => res.json(data))
    .catch((error) => res.json({ message: error }));
});

module.exports = router; 
