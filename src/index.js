const express = require("express");
const res = require("express/lib/response");
const mongoose = require("mongoose");
const userRoutes = require("./routes/clientes.js")
require("dotenv").config();

const app = express();
const port = process.env.PORT || 8080;

//middleware

app.use(express.json())
app.use("/api",userRoutes);

//routes
app.get("/", (req, res) => {
  res.send("hello word");
});

//mongodb connection
mongoose.connect("mongodb+srv://DemoGrupo66:58M5MFDpUcHsqeGd@grupo69.prgbouj.mongodb.net/grupo69?retryWrites=true&w=majority")
.then(() => {
  console.log("conectado con mongodb atlas");
})
.catch(()=>{
    console.log("error de conexion");
});

app.listen(port, () => console.log("servidor corriendo por el puerto ", port));
